<?php declare(strict_types=1);

namespace TeuCmsPack\Core\Cms;

use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\CriteriaCollection;
use Shopware\Core\Content\Cms\DataResolver\Element\AbstractCmsElementResolver;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\FieldConfig;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\Struct\ImageStruct;
use Shopware\Core\Content\Cms\SalesChannel\Struct\ProductBoxStruct;
use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Struct\ArrayEntity;

class HeroSliderCmsElementResolver extends AbstractCmsElementResolver
{
    public function getType(): string
    {
        return 'teu-heroslider'; // technischer Name des Elements
    }

    public function collect(CmsSlotEntity $slot, ResolverContext $resolverContext): ?CriteriaCollection
    {
        // Konfiguration des Elements auf der CMS Seite (Daten aus Administration)
        $config = $slot->getFieldConfig();

        // z.B. ein konfiguriertes Produkt, das nun aufgelöst werden soll
        $numberOfSlides =$config->get('numberOfSlides')->getValue();

        $productConfig = array();
        $criteriaCollection = new CriteriaCollection();
        for ($i =1; $i <= $numberOfSlides; $i++) {
            $mediaConfig[$i] = $config->get('media'.$i);
            // Wir müssen keine weiteren Daten anfragen wenn:
            //  1. die Config nicht existiert
            //  2. die Daten aus einem anderen Feld bezogen werden (mapped)
            //  3. keine Value für die Config existiert
            if (!$mediaConfig[$i] || $mediaConfig[$i]->isMapped() || $mediaConfig[$i]->getValue() === null) {
                continue;
            }

            $criteria = new Criteria([$mediaConfig[$i]->getValue()]);

            $criteriaCollection->add(
                'teu-heroslider-media'.$i, // einzigartiger Key, damit wir die Daten später wiederfinden
                MediaDefinition::class, // Auf welche Entität bezieht sich das Criteria Objekt
                $criteria
            );
        }


        return $criteriaCollection;

    }

    public function enrich(CmsSlotEntity $slot, ResolverContext $resolverContext, ElementDataCollection $result): void
    {

        $config = $slot->getFieldConfig();
        // Ein neues Objekt welches in der Storefront zur Verfügung steht
        //$data = new ProductBoxStruct();
        $data = new ArrayEntity();
        $slot->setData($data);
        //var_dump('test');die;

        $numberOfSlides =$config->get('numberOfSlides')->getValue();

        $orderSlides =array_filter(explode(',',(string) $config->get('sorting')->getValue()));


        $tempArticles = array();
        $tempDefaultOrder = array();
        for ($i =1; $i <= $numberOfSlides; $i++) {
            // Aus dem ResultSet das Produkt raus holen und in $data mappen
            /*$mediaConfig = $config->get('media');
            if ($mediaConfig && $mediaConfig->getValue()) {
                $this->addMediaEntity($slot, $image, $result, $mediaConfig, $resolverContext);
            }*/
            $media = $result->get('teu-heroslider-media'.$i)->first();
            $tempArticles[$i] = $media;
            array_push($tempDefaultOrder, $i);
        }
        if (empty($orderSlides)) {
            $orderSlides = $tempDefaultOrder;
        }

        $data->set('HerosliderImages', $tempArticles);
        $data->set('HerosliderOrder', $orderSlides);


        // die Daten stehen nun via Storefront + API bereit
    }

    private function addMediaEntity(CmsSlotEntity $slot, ImageStruct $image, ElementDataCollection $result,
                                    FieldConfig $config, ResolverContext $resolverContext): void
    {
        if ($config->isMapped() && $resolverContext instanceof EntityResolverContext) {
            /** @var MediaEntity|null $media */
            $media = $this->resolveEntityValue($resolverContext->getEntity(), $config->getValue());

            if ($media !== null) {
                $image->setMediaId($media->getUniqueIdentifier());
                $image->setMedia($media);
            }
        }

        if ($config->isStatic()) {
            $image->setMediaId($config->getValue());

            $searchResult = $result->get('media_' . $slot->getUniqueIdentifier());
            if (!$searchResult) {
                return;
            }

            /** @var MediaEntity|null $media */
            $media = $searchResult->get($config->getValue());
            if (!$media) {
                return;
            }

            $image->setMedia($media);
        }
    }
}
