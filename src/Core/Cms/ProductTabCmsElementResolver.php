<?php declare(strict_types=1);

namespace TeuCmsPack\Core\Cms;

use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\CriteriaCollection;
use Shopware\Core\Content\Cms\DataResolver\Element\AbstractCmsElementResolver;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\Struct\ProductBoxStruct;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Struct\ArrayEntity;

class ProductTabCmsElementResolver extends AbstractCmsElementResolver
{
    public function getType(): string
    {
        return 'teu-articletab'; // technischer Name des Elements
    }

    public function collect(CmsSlotEntity $slot, ResolverContext $resolverContext): ?CriteriaCollection
    {
        // Konfiguration des Elements auf der CMS Seite (Daten aus Administration)
        $config = $slot->getFieldConfig();

        // z.B. ein konfiguriertes Produkt, das nun aufgelöst werden soll
        $numberOfTabs =$config->get('numberOfTabs')->getValue();

        $productConfig = array();
        $criteriaCollection = new CriteriaCollection();
        for ($i =1; $i <= $numberOfTabs; $i++) {
            $productConfig[$i] = $config->get('tabArticle'.$i);
            // Wir müssen keine weiteren Daten anfragen wenn:
            //  1. die Config nicht existiert
            //  2. die Daten aus einem anderen Feld bezogen werden (mapped)
            //  3. keine Value für die Config existiert
            if (!$productConfig[$i] || $productConfig[$i]->isMapped() || $productConfig[$i]->getValue() === null) {
                continue;
            }

            $criteria = new Criteria([$productConfig[$i]->getValue()]);

            $criteriaCollection->add(
                'teu-articletab-article'.$i, // einzigartiger Key, damit wir die Daten später wiederfinden
                ProductDefinition::class, // Auf welche Entität bezieht sich das Criteria Objekt
                $criteria
            );
        }


        return $criteriaCollection;
    }

    public function enrich(CmsSlotEntity $slot, ResolverContext $resolverContext, ElementDataCollection $result): void
    {
        $config = $slot->getFieldConfig();
        // Ein neues Objekt welches in der Storefront zur Verfügung steht
        //$data = new ProductBoxStruct();
        $data = new ArrayEntity();
        $slot->setData($data);
        //var_dump('test');die;

        $numberOfTabs =$config->get('numberOfTabs')->getValue();
        //var_dump($data);die;
        $tempArticles = array();
        for ($i =1; $i <= $numberOfTabs; $i++) {
            // Aus dem ResultSet das Produkt raus holen und in $data mappen
            $product = $result->get('teu-articletab-article'.$i)->first();
            $tempArticles[$i] = $product;
        }
        $data->set('tabArticles', $tempArticles);



        // die Daten stehen nun via Storefront + API bereit
    }
}
