
// add new category
import './module/sw-cms/component/sw-cms-sidebar';

// add new elements
import './module/sw-cms/elements/videoslider';
import './module/sw-cms/elements/articletab';
import './module/sw-cms/elements/heroslider';
import './module/sw-cms/elements/hotspot';

// add new custom fields
import './app/component/form/swag-youtube-field';

// add new block
import './module/sw-cms/blocks/teufels/videoslider';
import './module/sw-cms/blocks/teufels/articletab';
import './module/sw-cms/blocks/teufels/heroslider';
import './module/sw-cms/blocks/teufels/hotspot';

import deDE from './module/sw-cms/snippet/de-DE.json';
import enGB from './module/sw-cms/snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
