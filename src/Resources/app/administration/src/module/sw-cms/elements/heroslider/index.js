import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'teu-heroslider',
    label: 'sw-cms.elements.customTeuHeroslider.label',
    component: 'sw-cms-el-heroslider',
    configComponent: 'sw-cms-el-config-heroslider',
    previewComponent: 'sw-cms-el-preview-heroslider',
    defaultConfig: {
        numberOfSlides: {
            source: 'static',
            value: 1
        },
        showControls: {
            source: 'static',
            value: true
        },

        navigationArrows: {
            source: 'static',
            value: 'outside'
        },
        navigationDots: {
            source: 'static',
            value: null
        },

        sorting: { source: 'static', value: '' },

        slideTitle1: { source: 'static', value: '' },
        slideTitle2: { source: 'static', value: '' },
        slideTitle3: { source: 'static', value: '' },
        slideTitle4: { source: 'static', value: '' },
        slideTitle5: { source: 'static', value: '' },
        slideTitle6: { source: 'static', value: '' },
        slideTitle7: { source: 'static', value: '' },
        slideTitle8: { source: 'static', value: '' },
        slideTitle9: { source: 'static', value: '' },
        slideTitle10: { source: 'static', value: '' },

        slideText1: { source: 'static', value: '' },
        slideText2: { source: 'static', value: '' },
        slideText3: { source: 'static', value: '' },
        slideText4: { source: 'static', value: '' },
        slideText5: { source: 'static', value: '' },
        slideText6: { source: 'static', value: '' },
        slideText7: { source: 'static', value: '' },
        slideText8: { source: 'static', value: '' },
        slideText9: { source: 'static', value: '' },
        slideText10: { source: 'static', value: '' },

        sliderLinkActive1: { source: 'static', value: false },
        sliderLinkActive2: { source: 'static', value: false },
        sliderLinkActive3: { source: 'static', value: false },
        sliderLinkActive4: { source: 'static', value: false },
        sliderLinkActive5: { source: 'static', value: false },
        sliderLinkActive6: { source: 'static', value: false },
        sliderLinkActive7: { source: 'static', value: false },
        sliderLinkActive8: { source: 'static', value: false },
        sliderLinkActive9: { source: 'static', value: false },
        sliderLinkActive10: { source: 'static', value: false },


        sliderBtnText1: { source: 'static', value: '' },
        sliderBtnText2: { source: 'static', value: '' },
        sliderBtnText3: { source: 'static', value: '' },
        sliderBtnText4: { source: 'static', value: '' },
        sliderBtnText5: { source: 'static', value: '' },
        sliderBtnText6: { source: 'static', value: '' },
        sliderBtnText7: { source: 'static', value: '' },
        sliderBtnText8: { source: 'static', value: '' },
        sliderBtnText9: { source: 'static', value: '' },
        sliderBtnText10: { source: 'static', value: '' },


        slideLink1: { source: 'static', value: '' },
        slideLink2: { source: 'static', value: '' },
        slideLink3: { source: 'static', value: '' },
        slideLink5: { source: 'static', value: '' },
        slideLink6: { source: 'static', value: '' },
        slideLink7: { source: 'static', value: '' },
        slideLink8: { source: 'static', value: '' },
        slideLink9: { source: 'static', value: '' },
        slideLink10: { source: 'static', value: '' },

        sliderLinkNewTab1: { source: 'static', value: false },
        sliderLinkNewTab2: { source: 'static', value: false },
        sliderLinkNewTab3: { source: 'static', value: false },
        sliderLinkNewTab4: { source: 'static', value: false },
        sliderLinkNewTab5: { source: 'static', value: false },
        sliderLinkNewTab6: { source: 'static', value: false },
        sliderLinkNewTab7: { source: 'static', value: false },
        sliderLinkNewTab8: { source: 'static', value: false },
        sliderLinkNewTab9: { source: 'static', value: false },
        sliderLinkNewTab10: { source: 'static', value: false },

        slideDisable1: { source: 'static', value: false },
        slideDisable2: { source: 'static', value: false },
        slideDisable3: { source: 'static', value: false },
        slideDisable4: { source: 'static', value: false },
        slideDisable5: { source: 'static', value: false },
        slideDisable6: { source: 'static', value: false },
        slideDisable7: { source: 'static', value: false },
        slideDisable8: { source: 'static', value: false },
        slideDisable9: { source: 'static', value: false },
        slideDisable10: { source: 'static', value: false },

        media: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media1: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media2: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media3: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media4: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media5: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media6: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media7: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media8: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media9: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
        media10: {
            source: 'static',
            value: null,
            entity: {
                name: 'media'
            }
        },
    }
});
