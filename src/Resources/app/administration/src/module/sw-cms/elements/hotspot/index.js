import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
	name: 'teu-hotspot',
	label: 'sw-cms.elements.customTeuHotspot.label',
	component: 'sw-cms-el-teuhotspot',
	configComponent: 'sw-cms-el-config-teuhotspot',
	previewComponent: 'sw-cms-el-preview-teuhotspot',
	defaultConfig: {
        numberOfHotspots: {
            source: 'static',
            value: 3
        },
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media'
            }
        },
        highlightcolor: {
            source: 'static',
            value: '#188EEE'
        },
        design: {
            source: 'static',
            value: 'einfach'
        },

        hotspotActiv1: { source: 'static', value: true },
        hotspotActiv2: { source: 'static', value: true },
        hotspotActiv3: { source: 'static', value: true },
        hotspotActiv4: { source: 'static', value: true },
        hotspotActiv5: { source: 'static', value: true },
        hotspotActiv6: { source: 'static', value: true },
        hotspotActiv7: { source: 'static', value: true },
        hotspotActiv8: { source: 'static', value: true },
        hotspotActiv9: { source: 'static', value: true },
        hotspotActiv10: { source: 'static', value: true },

        hotspotTitle1: { source: 'static', value: '' },
        hotspotTitle2: { source: 'static', value: '' },
        hotspotTitle3: { source: 'static', value: '' },
        hotspotTitle4: { source: 'static', value: '' },
        hotspotTitle5: { source: 'static', value: '' },
        hotspotTitle6: { source: 'static', value: '' },
        hotspotTitle7: { source: 'static', value: '' },
        hotspotTitle8: { source: 'static', value: '' },
        hotspotTitle9: { source: 'static', value: '' },
        hotspotTitle10: { source: 'static', value: '' },

        hotspotText1: { source: 'static', value: '' },
        hotspotText2: { source: 'static', value: '' },
        hotspotText3: { source: 'static', value: '' },
        hotspotText4: { source: 'static', value: '' },
        hotspotText5: { source: 'static', value: '' },
        hotspotText6: { source: 'static', value: '' },
        hotspotText7: { source: 'static', value: '' },
        hotspotText8: { source: 'static', value: '' },
        hotspotText9: { source: 'static', value: '' },
        hotspotText10: { source: 'static', value: '' },

        hotspotLinkActive1: { source: 'static', value: false },
        hotspotLinkActive2: { source: 'static', value: false },
        hotspotLinkActive3: { source: 'static', value: false },
        hotspotLinkActive4: { source: 'static', value: false },
        hotspotLinkActive5: { source: 'static', value: false },
        hotspotLinkActive6: { source: 'static', value: false },
        hotspotLinkActive7: { source: 'static', value: false },
        hotspotLinkActive8: { source: 'static', value: false },
        hotspotLinkActive9: { source: 'static', value: false },
        hotspotLinkActive10: { source: 'static', value: false },

        hotspotLinkUrl1: { source: 'static', value: '' },
        hotspotLinkUrl2: { source: 'static', value: '' },
        hotspotLinkUrl3: { source: 'static', value: '' },
        hotspotLinkUrl4: { source: 'static', value: '' },
        hotspotLinkUrl5: { source: 'static', value: '' },
        hotspotLinkUrl6: { source: 'static', value: '' },
        hotspotLinkUrl7: { source: 'static', value: '' },
        hotspotLinkUrl8: { source: 'static', value: '' },
        hotspotLinkUrl9: { source: 'static', value: '' },
        hotspotLinkUrl10: { source: 'static', value: '' },

        hotspotLinkNewTab1: { source: 'static', value: false },
        hotspotLinkNewTab2: { source: 'static', value: false },
        hotspotLinkNewTab3: { source: 'static', value: false },
        hotspotLinkNewTab4: { source: 'static', value: false },
        hotspotLinkNewTab5: { source: 'static', value: false },
        hotspotLinkNewTab6: { source: 'static', value: false },
        hotspotLinkNewTab7: { source: 'static', value: false },
        hotspotLinkNewTab8: { source: 'static', value: false },
        hotspotLinkNewTab9: { source: 'static', value: false },
        hotspotLinkNewTab10: { source: 'static', value: false },

        hotspotLinkText1: { source: 'static', value: '' },
        hotspotLinkText2: { source: 'static', value: '' },
        hotspotLinkText3: { source: 'static', value: '' },
        hotspotLinkText4: { source: 'static', value: '' },
        hotspotLinkText5: { source: 'static', value: '' },
        hotspotLinkText6: { source: 'static', value: '' },
        hotspotLinkText7: { source: 'static', value: '' },
        hotspotLinkText8: { source: 'static', value: '' },
        hotspotLinkText9: { source: 'static', value: '' },
        hotspotLinkText10: { source: 'static', value: '' },

        markerXpos1: { source: 'static', value: '20' },
        markerXpos2: { source: 'static', value: '20' },
        markerXpos3: { source: 'static', value: '20' },
        markerXpos4: { source: 'static', value: '20' },
        markerXpos5: { source: 'static', value: '20' },
        markerXpos6: { source: 'static', value: '20' },
        markerXpos7: { source: 'static', value: '20' },
        markerXpos8: { source: 'static', value: '20' },
        markerXpos9: { source: 'static', value: '20' },
        markerXpos10: { source: 'static', value: '20' },

        markerYpos1: { source: 'static', value: '40' },
        markerYpos2: { source: 'static', value: '40' },
        markerYpos3: { source: 'static', value: '40' },
        markerYpos4: { source: 'static', value: '40' },
        markerYpos5: { source: 'static', value: '40' },
        markerYpos6: { source: 'static', value: '40' },
        markerYpos7: { source: 'static', value: '40' },
        markerYpos8: { source: 'static', value: '40' },
        markerYpos9: { source: 'static', value: '40' },
        markerYpos10: { source: 'static', value: '40' },

        hoverDefaultPos1: { source: 'static', value: '' },
        hoverDefaultPos2: { source: 'static', value: '' },
        hoverDefaultPos3: { source: 'static', value: '' },
        hoverDefaultPos4: { source: 'static', value: '' },
        hoverDefaultPos5: { source: 'static', value: '' },
        hoverDefaultPos6: { source: 'static', value: '' },
        hoverDefaultPos7: { source: 'static', value: '' },
        hoverDefaultPos8: { source: 'static', value: '' },
        hoverDefaultPos9: { source: 'static', value: '' },
        hoverDefaultPos10: { source: 'static', value: '' },

        hoverPos1: { source: 'static', value: '' },
        hoverPos2: { source: 'static', value: '' },
        hoverPos3: { source: 'static', value: '' },
        hoverPos4: { source: 'static', value: '' },
        hoverPos5: { source: 'static', value: '' },
        hoverPos6: { source: 'static', value: '' },
        hoverPos7: { source: 'static', value: '' },
        hoverPos8: { source: 'static', value: '' },
        hoverPos9: { source: 'static', value: '' },
        hoverPos10: { source: 'static', value: '' },

	},

    defaultData: {
        media : '/administration/static/img/cms/preview_glasses_large.jpg',
    }
});
