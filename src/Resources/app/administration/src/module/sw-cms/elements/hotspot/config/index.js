const { Component, Mixin } = Shopware;
import template from './sw-cms-el-config-teuhotpot.html.twig';

Component.register('sw-cms-el-config-teuhotspot', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null
        };
    },

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        uploadTag() {
            return `cms-element-media-config-${this.element.id}`;
        },

        mediaUrl() {
            const context = Shopware.Context.api;
            const elemData = this.element.data.media;
            const mediaSource = this.element.config.media.source;

            if (elemData && elemData.id) {
                return this.element.data.media.url;
            }

            if (elemData && elemData.url) {
                return `${context.assetsPath}${elemData.url}`;
            }
        },

        previewSource() {
            if (this.element.data && this.element.data.media && this.element.data.media.id) {
                return this.element.data.media;
            }
            return this.element.config.media.value;
        },

    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-hotspot');
        },

        async onImageUpload({ targetId }) {
            const mediaEntity = await this.mediaRepository.get(targetId, Shopware.Context.api);

            this.element.config.media.value = mediaEntity.id;

            this.updateElementData(mediaEntity);

            this.$emit('element-update', this.element);
        },

        onImageRemove() {
            this.element.config.media.value = null;

            this.updateElementData();

            this.$emit('element-update', this.element);
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
        },

        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];
            this.element.config.media.value = media.id;

            this.updateElementData(media);

            this.$emit('element-update', this.element);
        },

        updateElementData(media = null) {
            this.$set(this.element.data, 'mediaId', media === null ? null : media.id);
            this.$set(this.element.data, 'media', media);
        },

        onOpenMediaModal() {
            this.mediaModalIsOpen = true;
        },

        setBgImage(event, currentTab = 1, highlightcol) {

            var currentCanvas = document.getElementById("canvas-hs-"+currentTab);
            var markerXPos = ((this.element.config['markerXpos' + currentTab].value)/100)*300;
            var markerYPos = ((this.element.config['markerYpos' + currentTab].value)/100)*150;

            var currentCanvasContext = currentCanvas.getContext("2d");

            // clear canvas to remove previous marker
            currentCanvasContext.clearRect(0, 0, currentCanvas.width, currentCanvas.height);

            // draw background image
            var currbgimg = new Image();

            // corrections because of media object null error
            // otherwise no preview images in tabs in category view
            var mediaId = this.element.config.media.value;
            this.mediaRepository.get(mediaId, Shopware.Context.api)
                .then((currMediaEntity) => {
                    currbgimg.src = currMediaEntity.url;

                    // draw note if image is null
                    if ((currbgimg.src).includes('undefined')){
                        currentCanvasContext.font = "13px Source Sans Pro";
                        currentCanvasContext.textAlign = "center";
                        currentCanvasContext.fillStyle = "#52667a";
                        currentCanvasContext.fillText("No image set", currentCanvas.width/2, currentCanvas.height/2);
                    }

                    currbgimg.onload = function(){
                        // set bg image
                        currentCanvasContext.drawImage(currbgimg, 0, 0, currbgimg.width, currbgimg.height, 0, 0, currentCanvas.width, currentCanvas.height);

                        // create marker as circle
                        var circleRadius = 10;
                        currentCanvasContext.beginPath();
                        currentCanvasContext.arc(markerXPos, markerYPos, circleRadius, 0, 2 * Math.PI);
                        currentCanvasContext.fillStyle = highlightcol;
                        currentCanvasContext.fill();
                        currentCanvasContext.lineWidth = 3;
                        currentCanvasContext.strokeStyle = 'white';
                        currentCanvasContext.stroke();
                    }
                });
        },

        setHotspot(event, currentTab = 1, highlightcol) {
            // get canvas and context
            var currentCanvas = event.target;
            var currentCanvasId = event.target.id;
            var currentCanvasContext = currentCanvas.getContext("2d");

            // get mouse position
            var boundingRect = event.target.getBoundingClientRect();
            var currMouseX = event.clientX - boundingRect.left;
            var currMouseY = event.clientY - boundingRect.top;

            // update marker values
            // process mouseposition for a good image mapping
            // canvas width is fixed: 300
            // canvas height is fixed: 150
            this.element.config['markerXpos' + currentTab].value = Math.round((currMouseX / 300)*100);
            this.element.config['markerYpos' + currentTab].value = Math.round((currMouseY / 150)*100);
            this.element.config['hoverDefaultPos' + currentTab].value = this.getDefaultHoverPos(currMouseX, currMouseY);

            // clear canvas to remove previous marker
            currentCanvasContext.clearRect(0, 0, currentCanvas.width, currentCanvas.height);

            // draw background image
            var currbgimg = new Image();

            // corrections because of media object null error
            // otherwise no preview images in tabs in category view
            var mediaId = this.element.config.media.value;
            this.mediaRepository.get(mediaId, Shopware.Context.api)
                .then((currMediaEntity) => {
                    currbgimg.src = currMediaEntity.url;

                    currbgimg.onload = function(){

                        // set bg image
                        currentCanvasContext.drawImage(currbgimg, 0, 0, currbgimg.width, currbgimg.height, 0, 0, currentCanvas.width, currentCanvas.height);

                        // create marker as circle
                        var circleRadius = 10;
                        currentCanvasContext.beginPath();
                        currentCanvasContext.arc(currMouseX, currMouseY, circleRadius, 0, 2 * Math.PI);
                        currentCanvasContext.fillStyle = highlightcol;
                        currentCanvasContext.fill();
                        currentCanvasContext.lineWidth = 3;
                        currentCanvasContext.strokeStyle = 'white';
                        currentCanvasContext.stroke();
                    }
                });
        },

        getDefaultHoverPos(xPos, yPos) {
            // get class to position the hover overlay
            if(xPos <= 50 && yPos <= 50){
                return 'bottom-right';
            }

            else if(xPos >= 50 && yPos <= 50){
                return 'bottom-left';
            }

            else if(xPos <= 50 && yPos >= 50){
                return 'top-right';
            }

            else {
                return 'top-left';
            }
        },
    }
});


