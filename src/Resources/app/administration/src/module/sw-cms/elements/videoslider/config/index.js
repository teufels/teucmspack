const { Component, Mixin, Context } = Shopware;
import template from './sw-cms-el-config-videoslider.html.twig';
import './sw-cms-el-config-videoslider.scss';

Component.register('sw-cms-el-config-videoslider', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    inject: ['repositoryFactory'],

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        activeSlides() {
            return this.element.config.slides.value.filter((slide) => {
                return slide.active
            });
        }
    },

    /*data() {
        return {
            active: 'video1'
        };
    },*/

    created() {
        this.createdComponent();
    },

    mounted() {
        //this.getMediaEntityPreviews();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-videoslider');
        },

        async asyncForEach(array, callback) {
            for (let i = 0; i < array.length; i++) {
                await callback(array[i], i, array);
            }
        },

        onClickAddSlide() {
            this.element.config.slides.value.push({
                active: true,
                name: 'New Slide',
                contentType: 'default',

                // video
                videoType: 'youtube',
                videoSrc: null,
                videoSrcImg: null,

                //content
                title: '',
                text:'',
                link:'',
                linkNewTab:'',
                linkText:''
            });

            //this.getMediaEntityPreviews();
        },

        onClickMoveSlide(index, direction) {
            const slides = this.element.config.slides.value;
            let newIndex = null;

            if (direction === 'up') {
                newIndex = index - 1;
            } else if (direction === 'down') {
                newIndex = index + 1;
            }

            if (newIndex !== null) {
                slides.splice(newIndex, 0, slides.splice(index, 1)[0]);
            }

            this.element.config.slides.value = slides;
            //this.getMediaEntityPreviews();
        },

        onClickDuplicateSlide(slide) {
            const slides = this.element.config.slides.value;
            const index = slides.indexOf(slide);
            const slideCopy = { ...slide };

            slideCopy.name += ' Kopie';
            slides.splice(index + 1, 0, slideCopy);
            this.element.config.slides.value = slides;
            //this.getMediaEntityPreviews();
        },

        onClickRemoveSlide(slide) {
            const slides = this.element.config.slides.value;
            const index = slides.indexOf(slide);

            slides.splice(index, 1);

            if (slides.length === 1) {
                slides[0].active = true;
            }

            this.element.config.slides.value = slides;
            //this.getMediaEntityPreviews();
        },

        setTimeValue(value, type) {
            this.element.config[type].value = this.convertTimeToUrlFormat(value).string;
        },

        convertTimeToInputFormat(time) {
            /* converting the time to a human readable format.
             * e.g. 1337 (seconds) -> 22:17
             */

            const returnValues = {};
            let incomingTime = time;

            const regex = /^[0-9]*$/;
            const isValidFormat = regex.test(time);

            if (!isValidFormat) {
                incomingTime = 0;
            }

            const minutes = Math.floor(incomingTime / 60);
            let seconds = incomingTime - minutes * 60;

            returnValues.minutes = minutes;
            returnValues.seconds = seconds;

            if (seconds.toString().length === 1) {
                seconds = `0${seconds}`;
            }

            returnValues.string = `${minutes}:${seconds}`;

            return returnValues;
        },

        convertTimeToUrlFormat(time) {
            /* converting the time to an url format so the YouTube iFrame-API can read the time.
             * e.g. 0:42 -> 42 (seconds)
             */

            const returnValues = {};
            let incomingTime = time;

            const regex = /[0-9]?[0-9]:[0-9][0-9]/;
            const isValidFormat = regex.test(incomingTime);

            if (!isValidFormat) {
                incomingTime = '00:00';
            }

            const splittedTime = incomingTime.split(':');
            returnValues.minutes = Number(splittedTime[0]);
            returnValues.seconds = Number(splittedTime[1]);
            returnValues.string = returnValues.minutes * 60 + returnValues.seconds;

            return returnValues;
        },
    }
});
