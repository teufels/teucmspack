const { Component } = Shopware;
import template from './sw-cms-el-preview-hotspot.html.twig';
import './sw-cms-el-preview-hotspot.scss';

Component.register('sw-cms-el-preview-teuhotspot', {
    template,
    computed: {
        assetFilter() {
            return Shopware.Filter.getByName('asset');
        },
    },
});
