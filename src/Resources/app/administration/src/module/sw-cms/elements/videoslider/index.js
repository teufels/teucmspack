import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'teu-videoslider',
    label: 'sw-cms.elements.customTeuVideoslider.label',
    component: 'sw-cms-el-videoslider',
    configComponent: 'sw-cms-el-config-videoslider',
    previewComponent: 'sw-cms-el-preview-videoslider',
    defaultConfig: {
        slides: {
            source: 'static',
            value: [
                {
                    active: true,
                    name: 'Slide 1',
                    contentType: 'default',

                    // video
                    videoType: 'youtube',
                    videoSrc: null,
                    videoSrcImg: null,
                    videoStart: null,
                    videoEnd: null,

                    //content
                    title: '',
                    text:'',
                    linkActive:false,
                    link:'',
                    linkNewTab:false,
                    linkText:''
                }
            ]
        },
        videoSettings: {
            source: 'static',
            value: {
                controls: true,
                loop: false,
                autoplay: false,
                mute: false,
                color: '#000'
            }
        },
        textSettings: {
            source: 'static',
            value: {
                show: true,
                position: 'absolute',
                textColSize: '',
            }
        },
        sliderSettings: {
            source: 'static',
            value: {
                //general
                mode: 'gallery',
                axis: 'horizontal',
                loop: false,
                rewind: false,
                autoHeight: false,
                mouseDrag: false,

                // navigation
                controls: true,
                controlsPos: 'outside',
                nav: true,
                navPos: 'outside',
            }
        }
    }
});
