const { Component, Mixin } = Shopware;
import template from './sw-cms-el-config-heroslider.html.twig';
import './sw-cms-el-config-heroslider.scss';

Component.register('sw-cms-el-config-heroslider', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null
        };
    },

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        uploadTag() {
            return `cms-element-media-config-${this.element.id}`;
        },

        /*previewSource() {
            if (this.element.data && this.element.data.media && this.element.data.media.id) {
                return this.element.data.media;
            }

            return this.element.config.media.value;
        }*/
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-heroslider');
        },

        previewSource(n) {
            if (this.element.data && this.element.data['media' + n] && this.element.data['media' + n].id) {
                return this.element.data['media' + n];
            }

            return this.element.config['media' + n].value;
        },


        onImageUpload({ targetId }, n) {
            this.mediaRepository.get(targetId, Shopware.Context.api).then((mediaEntity) => {
                this.element.config['media' + n].value = mediaEntity.id;
                this.updateElementData(n,mediaEntity);
                this.$emit('element-update', this.element);
            });
        },

        onImageRemove(n) {
            this.element.config['media' + n].value = null;
            this.updateElementData(null,n);
            this.$emit('element-update', this.element);
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
        },

        onSelectionChanges(mediaEntity, n) {
            const media = mediaEntity[0];
            this.element.config['media' + n].value = media.id;
            this.element.config['media' + n].source = 'static';
            this.updateElementData(media,n);
            this.$emit('element-update', this.element);
        },

        updateElementData(media = null,n) {
            this.$set(this.element.data, 'mediaId'+ n , media === null ? null : media.id);
            this.$set(this.element.data, 'media'+n , media);
        },

        onOpenMediaModal() {
            this.mediaModalIsOpen = true;
        },
    }
});
