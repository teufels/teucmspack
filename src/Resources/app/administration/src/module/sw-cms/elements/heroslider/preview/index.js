const { Component } = Shopware;
import template from './sw-cms-el-preview-heroslider.html.twig';
import './sw-cms-el-preview-heroslider.scss';

Component.register('sw-cms-el-preview-heroslider', {
    template,
    computed: {
        assetFilter() {
            return Shopware.Filter.getByName('asset');
        },
    },
});
