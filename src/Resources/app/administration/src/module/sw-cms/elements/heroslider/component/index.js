const { Component, Mixin } = Shopware;
import template from './sw-cms-el-heroslider.html.twig';
import './sw-cms-el-heroslider.scss';

Component.register('sw-cms-el-heroslider', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {
        SliderSrc1() {
            if (!this.element.data || !this.element.data.sliderSrc1) {
                return {
                    name: 'Lorem ipsum dolor',
                    description: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                    sed diam voluptua.`.trim(),
                    price: [
                        { gross: 19.90 }
                    ],
                    cover: {
                        media: {
                            url: '/administration/static/img/cms/preview_glasses_large.jpg',
                            alt: 'Lorem Ipsum dolor'
                        }
                    }
                };
            }

            return this.element.data.sliderSrc1;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-heroslider');
        }
    }
});
