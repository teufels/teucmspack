const { Component, Mixin } = Shopware;
import template from './sw-cms-el-hotspot.html.twig';
import './sw-cms-el-hotspot.scss';

Component.register('sw-cms-el-teuhotspot', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {
        mediaUrl() {

            const context = Shopware.Context.api;

            if (this.element.config.media.value) {
                if (this.element.data.media.id) {
                    return this.element.data.media.url;
                }

                return `${context.assetsPath}${element.data.media.url}`;
            }

            return `${context.assetsPath}/administration/static/img/cms/preview_mountain_large.jpg`;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-hotspot');
        }
    }
});
