const { Component, Mixin } = Shopware;
import template from './sw-cms-el-videoslider.html.twig';
import './sw-cms-el-videoslider.scss';

Component.register('sw-cms-el-videoslider', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {

    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-videoslider');
            this.initElementData('teu-videoslider');
        }
    }
});
