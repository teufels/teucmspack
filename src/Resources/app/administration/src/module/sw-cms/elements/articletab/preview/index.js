const { Component } = Shopware;
import template from './sw-cms-el-preview-articletab.html.twig';
import './sw-cms-el-preview-articletab.scss';

Component.register('sw-cms-el-preview-articletab', {
    template
});
