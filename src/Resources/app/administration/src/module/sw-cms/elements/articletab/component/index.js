const { Component, Mixin } = Shopware;
import template from './sw-cms-el-articletab.html.twig';
import './sw-cms-el-articletab.scss';

Component.register('sw-cms-el-articletab', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
        Mixin.getByName('placeholder')
    ],

    computed: {
        tabArticle1() {
            if (!this.element.data || !this.element.data.tabArticle1) {
                return {
                    name: 'Lorem ipsum dolor',
                    description: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                    sed diam voluptua.`.trim(),
                    price: [
                        { gross: 19.90 }
                    ],
                    cover: {
                        media: {
                            url: '/administration/static/img/cms/preview_glasses_large.jpg',
                            alt: 'Lorem Ipsum dolor'
                        }
                    }
                };
            }

            return this.element.data.tabArticle1;
        },
        tabArticle2() {
            if (!this.element.data || !this.element.data.tabArticle2) {
                return {
                    name: 'Lorem ipsum dolor',
                    description: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                    sed diam voluptua.`.trim(),
                    price: [
                        { gross: 19.90 }
                    ],
                    cover: {
                        media: {
                            url: '/administration/static/img/cms/preview_glasses_large.jpg',
                            alt: 'Lorem Ipsum dolor'
                        }
                    }
                };
            }

            return this.element.data.tabArticle2;
        },
        tabArticle3() {
            if (!this.element.data || !this.element.data.tabArticle3) {
                return {
                    name: 'Lorem ipsum dolor',
                    description: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                    sed diam voluptua.`.trim(),
                    price: [
                        { gross: 19.90 }
                    ],
                    cover: {
                        media: {
                            url: '/administration/static/img/cms/preview_glasses_large.jpg',
                            alt: 'Lorem Ipsum dolor'
                        }
                    }
                };
            }

            return this.element.data.tabArticle3;
        },
        tabArticle4() {
            if (!this.element.data || !this.element.data.tabArticle4) {
                return {
                    name: 'Lorem ipsum dolor',
                    description: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                    sed diam voluptua.`.trim(),
                    price: [
                        { gross: 19.90 }
                    ],
                    cover: {
                        media: {
                            url: '/administration/static/img/cms/preview_glasses_large.jpg',
                            alt: 'Lorem Ipsum dolor'
                        }
                    }
                };
            }

            return this.element.data.tabArticle4;
        },
        tabArticle5() {
            if (!this.element.data || !this.element.data.tabArticle5) {
                return {
                    name: 'Lorem ipsum dolor',
                    description: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                    sed diam voluptua.`.trim(),
                    price: [
                        { gross: 19.90 }
                    ],
                    cover: {
                        media: {
                            url: '/administration/static/img/cms/preview_glasses_large.jpg',
                            alt: 'Lorem Ipsum dolor'
                        }
                    }
                };
            }

            return this.element.data.tabArticle5;
        },

        mediaUrl1() {

            const context = Shopware.Context.api;

            if (this.tabArticle1.cover && this.tabArticle1.cover.media) {
                if (this.tabArticle1.cover.media.id) {
                    return this.tabArticle1.cover.media.url;
                }

                return `${context.assetsPath}${this.tabArticle1.cover.media.url}`;
            }

            return `${context.assetsPath}/administration/static/img/cms/preview_glasses_large.jpg`;
        },
        mediaUrl2() {

            const context = Shopware.Context.api;

            if (this.tabArticle2.cover && this.tabArticle2.cover.media) {
                if (this.tabArticle2.cover.media.id) {
                    return this.tabArticle2.cover.media.url;
                }

                return `${context.assetsPath}${this.tabArticle2.cover.media.url}`;
            }

            return `${context.assetsPath}/administration/static/img/cms/preview_glasses_large.jpg`;
        },
        mediaUrl3() {

            const context = Shopware.Context.api;

            if (this.tabArticle3.cover && this.tabArticle3.cover.media) {
                if (this.tabArticle3.cover.media.id) {
                    return this.tabArticle3.cover.media.url;
                }

                return `${context.assetsPath}${this.tabArticle3.cover.media.url}`;
            }

            return `${context.assetsPath}/administration/static/img/cms/preview_glasses_large.jpg`;
        },
        mediaUrl4() {

            const context = Shopware.Context.api;

            if (this.tabArticle4.cover && this.tabArticle4.cover.media) {
                if (this.tabArticle4.cover.media.id) {
                    return this.tabArticle4.cover.media.url;
                }

                return `${context.assetsPath}${this.tabArticle4.cover.media.url}`;
            }

            return `${context.assetsPath}/administration/static/img/cms/preview_glasses_large.jpg`;
        },
        mediaUrl5() {

            const context = Shopware.Context.api;

            if (this.tabArticle5.cover && this.tabArticle5.cover.media) {
                if (this.tabArticle5.cover.media.id) {
                    return this.tabArticle5.cover.media.url;
                }

                return `${context.assetsPath}${this.tabArticle5.cover.media.url}`;
            }

            return `${context.assetsPath}/administration/static/img/cms/preview_glasses_large.jpg`;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-articletab');
            this.initElementData('teu-articletab');
        }
    }
});
