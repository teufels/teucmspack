const { Component } = Shopware;
import template from './sw-cms-el-preview-videoslider.html.twig';
import './sw-cms-el-preview-videoslider.scss';

Component.register('sw-cms-el-preview-videoslider', {
    template
});
