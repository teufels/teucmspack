import './component';
import './config';
import './preview';

const Criteria = Shopware.Data.Criteria;
const criteria = new Criteria();
criteria.addAssociation('cover')

Shopware.Service('cmsService').registerCmsElement({
    name: 'teu-articletab',
    label: 'sw-cms.elements.customTeuArticleTab.label',
    component: 'sw-cms-el-articletab',
    configComponent: 'sw-cms-el-config-articletab',
    previewComponent: 'sw-cms-el-preview-articletab',
    defaultConfig: {
        numberOfTabs: {
            source: 'static',
            value: 3
        },
        tabArticle1: {
            source: 'static',
            value: null,
            entity: {
                name: 'product',
                criteria: criteria
            }
        },
        tabArticle2: {
            source: 'static',
            value: null,
            entity: {
                name: 'product',
                criteria: criteria
            }
        },
        tabArticle3: {
            source: 'static',
            value: null,
            entity: {
                name: 'product',
                criteria: criteria
            }
        },
        tabArticle4: {
            source: 'static',
            value: null,
            entity: {
                name: 'product',
                criteria: criteria
            }
        },
        tabArticle5: {
            source: 'static',
            value: null,
            entity: {
                name: 'product',
                criteria: criteria
            }
        },

        tabTitle1: { source: 'static', value: '' },
        tabTitle2: { source: 'static', value: '' },
        tabTitle3: { source: 'static', value: '' },
        tabTitle4: { source: 'static', value: '' },
        tabTitle5: { source: 'static', value: '' },

        tabText1: { source: 'static', value: '' },
        tabText2: { source: 'static', value: '' },
        tabText3: { source: 'static', value: '' },
        tabText4: { source: 'static', value: '' },
        tabText5: { source: 'static', value: '' },

        mediaUrl1: { source: 'static', value: '' },
        mediaUrl2: { source: 'static', value: '' },
        mediaUrl3: { source: 'static', value: '' },
        mediaUrl4: { source: 'static', value: '' },
        mediaUrl5: { source: 'static', value: '' },

    },
    defaultData: {
        tabArticle1: {
            name: 'Lorem Ipsum dolor',
            price: [
                { gross: 19.90 }
            ],
            cover: {
                media: {
                    url: '/administration/static/img/cms/preview_glasses_large.jpg',
                    alt: 'Lorem Ipsum dolor'
                }
            }
        },
        tabArticle2: {
            name: 'Lorem Ipsum dolor',
            price: [
                { gross: 19.90 }
            ],
            cover: {
                media: {
                    url: '/administration/static/img/cms/preview_glasses_large.jpg',
                    alt: 'Lorem Ipsum dolor'
                }
            }
        },
        tabArticle3: {
            name: 'Lorem Ipsum dolor',
            price: [
                { gross: 19.90 }
            ],
            cover: {
                media: {
                    url: '/administration/static/img/cms/preview_glasses_large.jpg',
                    alt: 'Lorem Ipsum dolor'
                }
            }
        },
        tabArticle4: {
            name: 'Lorem Ipsum dolor',
            price: [
                { gross: 19.90 }
            ],
            cover: {
                media: {
                    url: '/administration/static/img/cms/preview_glasses_large.jpg',
                    alt: 'Lorem Ipsum dolor'
                }
            }
        },
        tabArticle5: {
            name: 'Lorem Ipsum dolor',
            price: [
                { gross: 19.90 }
            ],
            cover: {
                media: {
                    url: '/administration/static/img/cms/preview_glasses_large.jpg',
                    alt: 'Lorem Ipsum dolor'
                }
            }
        }

    },
    collect: function collect(elem) {
        const context = Object.assign(
            {},
            Shopware.Context.api,
            { inheritance: true }
        );

        const criteriaList = {};

        Object.keys(elem.config).forEach((configKey) => {
            if (elem.config[configKey].source === 'mapped') {
                return;
            }

            const entity = elem.config[configKey].entity;

            if (entity && elem.config[configKey].value) {
                const entityKey = entity.name;
                const entityData = {
                    value: [elem.config[configKey].value],
                    key: configKey,
                    searchCriteria: entity.criteria ? entity.criteria : new Criteria(),
                    ...entity
                };

                entityData.searchCriteria.setIds(entityData.value);
                entityData.context = context;

                criteriaList[`entity-${entityKey}`] = entityData;
            }
        });

        return criteriaList;
    }
});
