const { Criteria } = Shopware.Data;
const { Component, Mixin } = Shopware;
import template from './sw-cms-el-config-articletab.html.twig';

Component.register('sw-cms-el-config-articletab', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    inject: ['repositoryFactory'],

    computed: {
        productRepository() {
            return this.repositoryFactory.create('product');
        },

        productSelectContext() {
            const context = Object.assign({}, Shopware.Context.api);
            context.inheritance = true;

            return context;
        }
    },


    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-articletab');
        },

        onProductChange(productId, n) {
            if (!productId) {
                //this.element.config.product.value = null;
                //this.$set(this.element.data[n], 'productId', null);
                this.$set(this.element.data, 'tabArticle'+n, null);
            } else {
                const criteria = new Criteria();
                criteria.addAssociation('cover');

                this.productRepository.get(productId, this.productSelectContext, criteria).then((product) => {
                    this.element.config['tabArticle' + n].value = productId;
                    this.$set(this.element.data, 'tabArticleId'+n, productId);
                    this.$set(this.element.data, 'tabArticle'+n, product);
                });
            }

            this.$emit('element-update', this.element);
        }
    }
});
