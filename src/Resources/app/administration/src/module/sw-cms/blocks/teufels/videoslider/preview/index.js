const { Component } = Shopware;
import template from './sw-cms-preview-teu-videoslider.html.twig';
import './sw-cms-preview-teu-videoslider.scss';

Component.register('sw-cms-preview-teu-videoslider', {
    template
});
