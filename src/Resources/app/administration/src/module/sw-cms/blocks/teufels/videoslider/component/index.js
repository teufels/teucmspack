const { Component } = Shopware;
import template from './sw-cms-block-teu-videoslider.html.twig';

Component.register('sw-cms-block-teu-videoslider', {
    template
});
