const { Component } = Shopware;
import template from './sw-cms-preview-teu-hotspot.html.twig';
import './sw-cms-preview-teu-hotspot.scss';

Component.register('sw-cms-preview-teu-hotspot', {
    template,
    computed: {
        assetFilter() {
            return Shopware.Filter.getByName('asset');
        },
    },
});
