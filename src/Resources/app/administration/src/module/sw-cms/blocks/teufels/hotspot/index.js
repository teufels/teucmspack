import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'teu-hotspot',
    label: 'sw-cms.elements.customTeuHotspot.label',
    category: 'teu-cmspack',
    component: 'sw-cms-block-teu-hotspot',
    previewComponent: 'sw-cms-preview-teu-hotspot',

    defaultConfig: {
        marginBottom: '20px',
        marginTop:    '20px',
        marginLeft:   '20px',
        marginRight:  '20px',
        sizingMode:   'boxed'
    },

    slots: {
        content: {
            type: 'teu-hotspot',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' }
                }
            }
        }
    }
});
