const { Component } = Shopware;
import template from './sw-cms-block-teu-articletab.html.twig';

Component.register('sw-cms-block-teu-articletab', {
    template
});
