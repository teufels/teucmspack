const { Component } = Shopware;
import template from './sw-cms-block-teu-hotspot.html.twig';

Component.register('sw-cms-block-teu-hotspot', {
    template
});
