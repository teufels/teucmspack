import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'teu-videoslider',
    label: 'sw-cms.elements.customTeuVideoslider.label',
    category: 'teu-cmspack',
    component: 'sw-cms-block-teu-videoslider',
    previewComponent: 'sw-cms-preview-teu-videoslider',

    defaultConfig: {
        marginBottom: '20px',
        marginTop:    '20px',
        marginLeft:   '20px',
        marginRight:  '20px',
        sizingMode:   'boxed'
    },

    slots: {
        content: {
            type: 'teu-videoslider'
        }
    }
});
