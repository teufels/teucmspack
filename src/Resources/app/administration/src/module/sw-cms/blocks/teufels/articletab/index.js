import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'teu-articletab',
    label: 'sw-cms.elements.customTeuArticleTab.label',
    category: 'teu-cmspack',
    component: 'sw-cms-block-teu-articletab',
    previewComponent: 'sw-cms-preview-teu-articletab',

    defaultConfig: {
        marginBottom: '20px',
        marginTop:    '20px',
        marginLeft:   '20px',
        marginRight:  '20px',
        sizingMode:   'boxed'
    },

    slots: {
        content: {
            type: 'teu-articletab'
        }
    }
});
