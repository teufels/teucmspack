const { Component } = Shopware;
import template from './sw-cms-preview-teu-articletab.html.twig';
import './sw-cms-preview-teu-articletab.scss';

Component.register('sw-cms-preview-teu-articletab', {
    template
});
