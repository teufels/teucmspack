import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'teu-heroslider',
    label: 'sw-cms.elements.customTeuHeroslider.label',
    category: 'teu-cmspack',
    component: 'sw-cms-block-teu-heroslider',
    previewComponent: 'sw-cms-preview-teu-heroslider',

    defaultConfig: {
        marginBottom: '20px',
        marginTop:    '20px',
        marginLeft:   '20px',
        marginRight:  '20px',
        sizingMode:   'boxed'
    },

    slots: {
        content: {
            type: 'teu-heroslider'
        }
    }
});
