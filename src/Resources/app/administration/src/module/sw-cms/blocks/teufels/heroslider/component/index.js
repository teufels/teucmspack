const { Component } = Shopware;
import template from './sw-cms-block-teu-heroslider.html.twig';

Component.register('sw-cms-block-teu-heroslider', {
    template
});
