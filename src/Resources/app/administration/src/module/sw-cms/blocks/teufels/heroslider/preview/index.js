const { Component } = Shopware;
import template from './sw-cms-preview-teu-heroslider.html.twig';
import './sw-cms-preview-teu-heroslider.scss';

Component.register('sw-cms-preview-teu-heroslider', {
    template,

    computed: {
        assetFilter() {
            return Shopware.Filter.getByName('asset');
        },
    },

});
