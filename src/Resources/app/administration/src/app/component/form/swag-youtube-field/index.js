const { Component } = Shopware;
import template from './swag-youtube-field.html.twig';

Component.register('swag-youtube-field', {
    template,

    props: {
        value: {
            type: String,
            required: false,
            default: ''
        }
    },

    data() {
        return {
            currentValue: this.value || ''
        };
    },

    methods: {
        onFetchIdFromUrl(url) {
            if (url.indexOf('vimeo') == -1) {
                var videoSrcRegex = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
                var id = url.replace(videoSrcRegex, '$1');
            } else {
                var videoSrcRegex = /(http|https)?:\/\/(www\.|player\.)?vimeo\.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|video\/|)(\d+)(?:|\/\?)/
                var id = url.replace(videoSrcRegex, '$4');
            }


            this.currentValue = id;
            this.$emit('input', this.currentValue);
        }
    }
});
