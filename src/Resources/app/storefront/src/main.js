import TeuCmspackTeuhotspot from "./teu-cmspack/teuhotspot/teu-cmspack-teuhotspot.plugin";
import TeuVideosliderPlugin from './teu-cmspack/teuVideoslider/teu-cms-teuvideoslider.plugin';


const PluginManager = window.PluginManager;
PluginManager.register('TeuCmspackTeuhotspot', TeuCmspackTeuhotspot, '[data-teu-cmspack-teuhotspot]');
PluginManager.register('TeuVideosliderPlugin', TeuVideosliderPlugin, '[data-teuVideo-slider]');

if (module.hot) {
    module.hot.accept();
}



