import Plugin from 'src/plugin-system/plugin.class';

export default class TeuCmspackTeuhotspot extends Plugin {
    static options = {
        /**
         * Specifies the text that is prompted to the user
         * @type string
         */
    };

    init() {
        const that = this;
        const hotspotContainer = document.querySelectorAll('[data-teu-cmspack-teuhotspot]');
        [].forEach.call(hotspotContainer, (node) => {
            node.closest('.cms-section').classList.add('overflow-visible');
        });
    }
}
