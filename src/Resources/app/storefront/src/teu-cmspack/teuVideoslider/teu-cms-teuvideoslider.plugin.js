
import { tns } from 'tiny-slider/src/tiny-slider.module';
import BaseSliderPlugin from 'src/plugin/slider/base-slider.plugin';
import SliderSettingsHelper from 'src/plugin/slider/helper/slider-settings.helper';
import ViewportDetection from 'src/helper/viewport-detection.helper';
import PluginManager from 'src/plugin-system/plugin.manager';

export default class TeuVideosliderPlugin extends BaseSliderPlugin {

    static options = {
        initializedCls: 'js-slider-initialized',
        containerSelector: '[data-base-slider-container=true]',
        controlsSelector: '[data-base-slider-controls=true]',
        slider: {
            enabled: true,
            responsive: {
                xs: {},
                sm: {},
                md: {},
                lg: {},
                xl: {},
            },
        },
    };

    init() {
        this._slider = false;

        if (!this.el.classList.contains(this.options.initializedCls)) {
            let config = JSON.parse(this.el.dataset.teuvideoSliderOptions);
            this.options.slider = Object.assign(this.options.slider,config.slider)
            this.options.slider = SliderSettingsHelper.prepareBreakpointPxValues(this.options.slider);

            super._correctIndexSettings();

            super._getSettings(ViewportDetection.getCurrentViewport());

            super._initSlider();
            super._registerEvents();
        }
        this.addEventListeners();
    }

    addEventListeners() {
        this._slider.events.on('transitionStart', () => {
            this.pauseAllYoutube();
            this.pauseAllVimeo();
        });
    }

    pauseAllYoutube() {
        const youtubeVideos = document.querySelectorAll('.cms-element-teu-youtube iframe[src*="youtube-nocookie.com"]');
        [].forEach.call(youtubeVideos, (node) => {
            node.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        });
    }

    pauseAllVimeo() {
        const youtubeVideos = document.querySelectorAll('.cms-element-teu-youtube iframe[src*="player.vimeo.com"]');
        [].forEach.call(youtubeVideos, (node) => {
            node.contentWindow.postMessage('{"method": "pause"}', '*');
        });
    }

}
